import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyComponent } from './lazy/lazy.component';
import { RouterModule } from '@angular/router';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';
import { TestModule } from '../../../projects/test/src/lib/test.module';


@NgModule({
    declarations: [LazyComponent, OneComponent, TwoComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: LazyComponent
            },
            {
                path: 'one',
                component: OneComponent
            },
            {
                path: 'two',
                component: TwoComponent
            }
        ]),
        TestModule
    ]
})
export class LazyModule {
}
