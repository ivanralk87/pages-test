import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: 'other',
                pathMatch: 'full'
            },
            {
                path: 'lazy',
                loadChildren: () => import('./lazy/lazy.module').then(m => m.LazyModule)
            },
            {
                path: 'other',
                loadChildren: () => import('./other/other.module').then(m => m.OtherModule)
            }
        ])
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
